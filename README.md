# Machine Learning Autocompleter

Fork of the [Galois Autocompleter project](https://github.com/galois-autocompleter/galois-autocompleter)
project, implementing an autocomplete muse for IDAS.

# Installation

## With Docker

You may pull pre-built GPU Docker image from the registry using the command
below:

```
docker pull registry.gitlab.com/grammatech/mnemosyne/argot-server/autocomplete
```

The GPU image assumes you have a Nvidia graphics card with a version of CUDA
installed compatible with tensorflow version == 1.14 (generally CUDA
version 10.0).  If this is not the case, the image will fall back to using
the CPU and responses will be slower.

Once pulled, you may start the docker image with the command:
`docker run --rm --network=host [--gpus all] <image-name>`.  This will
start an autocomplete language server listening on port 3030.  The
`--gpus all` argument should be elided if you are running the image
on a machine without a GPU(s).

## Without Docker (for Development/Debugging purposes)

To execute the autocompleter, you will need Python 3.6 or earlier to
support tensorflow version <= 1.14.

To begin, download the latest model from releases and
uncompress it into the repository's base directory:

```sh
wget https://github.com/iedmrc/galois-autocompleter/releases/latest/download/model.tar.xz
tar xf model.tar.xz
rm model.tar.xz
```

Once the model has been downloaded, install dependencies:
```sh
pip3 install -r requirements.txt
```

Finally, run the autocompleter:
```sh
./autocomplete
```

This will start an autocomplete language server listening on port 3030.

### Help

Executing `./autocomplete --help` will display a list of parameters
to the autocompleter script along with a brief description.  By default,
the script will start a language server listening on port 3030, but
it may also be used a RESTful web service by passing `--rest`.  Additionally,
a `--debug` flag is provided for additional debugging information.

#### FAQ

##### I am getting the error "failed call to cuInit: UNKNOWN ERROR" on startup

This is a non-fatal error which occurs when a Nvidia GPU with CUDA 10.0
installed is not found.  Tensorflow will fall back to using the CPU in
this case and responses will be slower.

##### I am getting the error "could not select device driver "" with capabilities: [[gpu]]."

This error occurs when `--gpus all` is passed to `docker run` for the
autocomplete image and no GPUs are present on the machine.  This argument
can be elided and the image will start successfully.

# Rebuilding Production Docker Image

The production docker image may be rebuilt manually be executing the following
command:

```
docker build registry.gitlab.com/grammatech/mnemosyne/argot-server/autocomplete
docker push registry.gitlab.com/grammatech/mnemosyne/argot-server/autocomplete
```

In the future this process will be automated in the continuous integration
process.
